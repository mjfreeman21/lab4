import aima.core.learning.learners.DecisionTreeLearner;
import aima.core.learning.framework.DataSetFactory;
import aima.core.learning.framework.DataSetSpecification;
import aima.core.learning.framework.DataSet;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;

public class Main {

	private static final String FIELDSEPERATOR = " ";

	/**
	 * setup the specification for the data set
	 */
	public static DataSetSpecification getSpecification() {
		// the dataset contains 6 attributes (a1, a2, ..., a6) with varying numbers of classes
		DataSetSpecification dss = new DataSetSpecification();
		dss.defineStringAttribute("class", new String[] { "0", "1" });
		dss.defineStringAttribute("a1", new String[] { "1", "2", "3" });
		dss.defineStringAttribute("a2", new String[] { "1", "2", "3" });
		dss.defineStringAttribute("a3", new String[] { "1", "2" });
		dss.defineStringAttribute("a4", new String[] { "1", "2", "3" });
		dss.defineStringAttribute("a5", new String[] { "1", "2", "3", "4" });
		dss.defineStringAttribute("a6", new String[] { "1", "2" });
		dss.setTarget("class");
		return dss;
	}

	/**
	 * load at most maxNbLines lines from a file named filename + ".csv" and return a data set made from those lines
	 */
	public static DataSet loadDataSet(String filename, DataSetSpecification spec, int maxNbLines) throws Exception {
		DataSet ds = new DataSet(spec);
		// open file
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filename + ".csv")));
		String line;
		// read file line by line and parse each line into an example that is added to the data set
		while (maxNbLines>0 && (line = reader.readLine()) != null) {
			ds.add(DataSetFactory.exampleFromString(line, spec, FIELDSEPERATOR));
			maxNbLines--;
		}
		reader.close();
		return ds;
	}

	/**
	 * like loadDataSet, but loads all lines in a file
	 */
	public static DataSet loadCompleteDataSet(String filename, DataSetSpecification spec) throws Exception {
		return loadDataSet(filename, spec, Integer.MAX_VALUE);
	}

	/**
	 * main procedure
	 */
	public static void main(String[] args) throws Exception {
		// setup the DataSetSpecification
		DataSetSpecification dss = getSpecification();

		/* load training and test data sets
		   There are three data sets describing different concepts:
		     monks-1 (124 training examples), monks-2 (169 training examples), monks-3 (122 training examples).
		   Look at the end of monks.names, to see which concepts the data sets describe.
		*/
		String dataSetName = "monks-1"; // other sets are "monks-2" and "monks-3"
		DataSet testData = loadCompleteDataSet(dataSetName + ".test", dss);
		DataSet trainingData = loadCompleteDataSet(dataSetName + ".train", dss);
		DecisionTreeLearner dtl = new DecisionTreeLearner();
		System.out.println("Monks 1");
		for (int i =10; i<125;i=i+10) {
			testData = loadCompleteDataSet(dataSetName + ".test", dss);
			trainingData = loadDataSet(dataSetName + ".train", dss,i);
			//System.out.println("Learning decision tree for " + dataSetName + " from " + trainingData.size() + " examples ...");
			dtl.train(trainingData);
			int[] testResult = dtl.test(testData);
			int nbCorrect = testResult[0];
			int nbIncorrect = testResult[1];
			System.out.println("test result for size: "+ trainingData.size() + " (" + (nbCorrect * 100.0 / (nbCorrect + nbIncorrect)) + "%)");

		}
		dataSetName = "monks-2"; // other sets are "monks-2" and "monks-3"
		System.out.println("Monks 2");

		for (int i =10; i<180;i=i+10) {
			testData = loadCompleteDataSet(dataSetName + ".test", dss);
			trainingData = loadDataSet(dataSetName + ".train", dss,i);
			//System.out.println("Learning decision tree for " + dataSetName + " from " + trainingData.size() + " examples ...");
			dtl.train(trainingData);
			int[] testResult = dtl.test(testData);
			int nbCorrect = testResult[0];
			int nbIncorrect = testResult[1];
			System.out.println("test result for size: "+ trainingData.size() + " (" + (nbCorrect * 100.0 / (nbCorrect + nbIncorrect)) + "%)");

		}
		System.out.println("Monks 3");
		dataSetName = "monks-3"; // other sets are "monks-2" and "monks-3"
		for (int i =10; i<130;i=i+10) {
			testData = loadCompleteDataSet(dataSetName + ".test", dss);
			trainingData = loadDataSet(dataSetName + ".train", dss,i);
			//System.out.println("Learning decision tree for " + dataSetName + " from " + trainingData.size() + " examples ...");
			dtl.train(trainingData);
			int[] testResult = dtl.test(testData);
			int nbCorrect = testResult[0];
			int nbIncorrect = testResult[1];
			System.out.println("test result for size: "+ trainingData.size() + " (" + (nbCorrect * 100.0 / (nbCorrect + nbIncorrect)) + "%)");

		}
		/*
		 * test result for finding consistency
		 */
		dataSetName = "monks-1";
		System.out.println("consistency monk- 1");
		testData = loadCompleteDataSet(dataSetName + ".train", dss);
		trainingData = loadCompleteDataSet(dataSetName + ".train", dss);
		dtl.train(trainingData);
		int[] testResult = dtl.test(testData);
		int nbCorrect = testResult[0];
		int nbIncorrect = testResult[1];
		System.out.println("test result for size: "+ trainingData.size() + " (" + (nbCorrect * 100.0 / (nbCorrect + nbIncorrect)) + "%)");
		
		
		
		System.out.println("consistency monk- 2");
		dataSetName = "monks-2";
		testData = loadCompleteDataSet(dataSetName + ".train", dss);
		trainingData = loadCompleteDataSet(dataSetName + ".train", dss);
		dtl.train(trainingData);
		testResult = dtl.test(testData);
		nbCorrect = testResult[0];
		nbIncorrect = testResult[1];
		System.out.println("test result for size: "+ trainingData.size() + " (" + (nbCorrect * 100.0 / (nbCorrect + nbIncorrect)) + "%)");

		System.out.println("consistency monk- 3");
		dataSetName = "monks-3";
		testData = loadCompleteDataSet(dataSetName + ".train", dss);
		trainingData = loadCompleteDataSet(dataSetName + ".train", dss);
		dtl.train(trainingData);
		testResult = dtl.test(testData);
		nbCorrect = testResult[0];
		nbIncorrect = testResult[1];
		System.out.println("test result for size: "+ trainingData.size() + " (" + (nbCorrect * 100.0 / (nbCorrect + nbIncorrect)) + "%)");
		
		//Decision Tree test
		System.out.println("monk-1 Decision Tree");
		dataSetName = "monks-1";
		//testData = loadCompleteDataSet(dataSetName + ".train", dss);
		trainingData = loadCompleteDataSet(dataSetName + ".train", dss);
		dtl.train(trainingData);
		System.out.println("the learned tree:");
		System.out.println(dtl.getDecisionTree());

		
		// Hint: use loadDataSet(...) instead of loadCompleteDataSet(...) to load only some of the examples from a file
		
		// initialize the learner

		// learn the decision tree using the training data

		// test the decision tree using the test data

		// print out the learned tree
		//System.out.println("the learned tree:");
		//System.out.println(dtl.getDecisionTree());
	}
}
